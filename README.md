# Decision Tree PoC

A simple PoC for building decision trees in Drupal.

## Installation
- `composer config repositories.'z.a.horvath/decision_tree' gitlab https://gitlab.com/z.a.horvath/decision_tree.git`
- `composer require z.a.horvath/decision_tree`
