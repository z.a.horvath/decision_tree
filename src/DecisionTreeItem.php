<?php

declare(strict_types=1);

namespace Drupal\decision_tree;

use Drupal\Component\Utility\Xss;
use Drupal\taxonomy\Entity\Term;

/**
 * Bundle class of decision tree item terms.
 */
class DecisionTreeItem extends Term {

  /**
   * Stores flag about whether the entity is built by the entity form.
   *
   * @var bool
   */
  protected $isInForm = FALSE;

  /**
   * Whether the entity is built by the entity form.
   *
   * @return bool
   *   TRUE if the entity is built by the entity form, FALSE otherwise.
   */
  public function isInForm(bool $isInForm = NULL): bool {
    if (isset($isInForm)) {
      $this->isInForm = $isInForm;
    }
    return $this->isInForm;
  }

  /**
   * Returns the main title of the Decision Tree item.
   *
   * @return string
   *   The main title of the Decision Tree item, containing both the option and
   *   the question.
   */
  public function getMainTitle() {
    $titlePieces = [
      $this->isAnswer() ? '✅' : '❓',
      $this->label(),
    ];
    if (!$this->isFirstQuestion()) {
      $titlePieces[] = '➡️';
      $titlePieces[] = $this->getQuestion() ?? $this->getAnswer();
    }
    $labelRenderable = [
      '#markup' => implode(" ", $titlePieces),
      '#allowed_tags' => Xss::getHtmlTagList(),
    ];

    return \Drupal::service('renderer')->renderPlain($labelRenderable);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->isInForm() ? parent::getName() : $this->getMainTitle();
  }

  /**
   * Returns the question.
   *
   * @return string|null
   *   The question, if any.
   */
  public function getQuestion(): ?string {
    return $this->isAnswer() ? NULL : $this->getDescription();
  }

  /**
   * Returns the answer.
   *
   * @return string|null
   *   The answer, if any.
   */
  public function getAnswer(): ?string {
    return $this->isAnswer() ? $this->getDescription() : NULL;
  }

  /**
   * Terms which have no parent.
   *
   * @return bool
   *   Whether the item is the top-most question.
   */
  public function isFirstQuestion(): bool {
    return empty($this->get('parent')->first()?->target_id);
  }

  /**
   * Terms which have no children.
   *
   * @return bool
   *   Whether the item is the bottom-most answer.
   */
  public function isAnswer(): bool {
    return $this->get('children')->isEmpty();
  }

}
