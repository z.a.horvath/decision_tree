<?php

declare(strict_types=1);

namespace Drupal\decision_tree\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\taxonomy\TermInterface;

/**
 * Computed field with the child terms of the current term.
 */
class ChildTerms extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue(): void {
    $term = $this->getEntity();
    assert($term instanceof TermInterface);

    $childrenIds = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->getQuery()
      ->accessCheck(TRUE)
      ->condition('parent', $this->getEntity()->id())
      ->execute();
    foreach ($childrenIds as $childId) {
      parent::appendItem(['target_id' => $childId]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update): bool {
    // Force recompute after save.
    $this->valueComputed = FALSE;
    $this->list = [];
    return parent::postSave($update);
  }

}
