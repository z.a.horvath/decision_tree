<?php

declare(strict_types=1);

namespace Drupal\decision_tree\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\decision_tree\DecisionTreeItem;

/**
 * Field revealing the top-most item's label (this is the main question).
 */
class DecisionTreeLabel extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue(): void {
    $entity = $this->getEntity();
    assert($entity instanceof DecisionTreeItem);
    while (!$entity->isFirstQuestion()) {
      $entity = $entity->get('parent')->first()->entity;
    }
    parent::appendItem(['value' => $entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update): bool {
    // Force recompute after save.
    $this->valueComputed = FALSE;
    $this->list = [];
    return parent::postSave($update);
  }

}
