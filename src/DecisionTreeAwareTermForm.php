<?php

declare(strict_types=1);

namespace Drupal\decision_tree;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\TermForm;

/**
 * Term form replacement.
 */
class DecisionTreeAwareTermForm extends TermForm {

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    if ($this->entity instanceof DecisionTreeItem) {
      $this->entity->isInForm(TRUE);
    }

    return parent::buildEntity($form, $form_state);
  }

}
